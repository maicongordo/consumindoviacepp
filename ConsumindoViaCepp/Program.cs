﻿using ConsumindoViaCepp.Models;
using Newtonsoft.Json;
using System;
using System.Net;

namespace ConsumindoViaCepp
{
    class Program
    {
        static void Main(string[] args)
        {
          //  string url = "http://viacep.com.br/ws/01001000/json/";

            Console.WriteLine("Digite o CEP para a pesquisa: ");
            string resp = Console.ReadLine();
            Console.Write("\n");

            string resultado = string.Format("http://viacep.com.br/ws/{0}/json/", resp );


            Cep cep = BuscarCep(resultado);
        

            Console.ReadLine();
        }


        public static Cep BuscarCep (string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.Write("\n \n");

            Cep Cep = JsonConvert.DeserializeObject<Cep>(content);
            return Cep;
        }
    }
}
